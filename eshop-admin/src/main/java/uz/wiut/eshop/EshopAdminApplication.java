package uz.wiut.eshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EshopAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(EshopAdminApplication.class,args);
    }
}
