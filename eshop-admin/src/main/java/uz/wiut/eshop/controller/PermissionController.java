package uz.wiut.eshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.wiut.eshop.entity.Permission;
import uz.wiut.eshop.security.SecurityService;
import uz.wiut.eshop.security.SecurityUtil;

import java.util.List;

@Controller
@Secured(SecurityUtil.MANAGE_PERMISSIONS)
public class PermissionController extends EshopAdminBaseController {

    private static final String viewPrefix = "permissions/";

    @Autowired
    protected SecurityService securityService;

    @Override
    protected String getHeaderTitle() {
        return "Manage Permissions";
    }

    @RequestMapping(value="/permissions", method=RequestMethod.GET)
    public String listPermissions(Model model) {
        List<Permission> list = securityService.findAllPermissions();
        model.addAttribute("permissions",list);
        return viewPrefix+"permissions";
    }


}
