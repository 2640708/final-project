package uz.wiut.eshop.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import uz.wiut.eshop.entity.Category;
import uz.wiut.eshop.service.CatalogService;

@Component
public class CategoryValidator implements Validator {
    @Autowired
    protected MessageSource messageSource;
    @Autowired
    protected CatalogService catalogService;


    @Override
    public boolean supports(Class<?> aClass) {
        return Category.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Category category = (Category) o;
        String name = category.getName();
        Category categoryByName = catalogService.getCategoryByName(name);
        if (categoryByName != null) {
            errors.rejectValue("name", "error.exists", new Object[]{name}, "Category " + category.getName() + " already exists");
        }
    }
}
