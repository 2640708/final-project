package uz.wiut.eshop.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import uz.wiut.eshop.entity.Product;
import uz.wiut.eshop.model.ProductForm;
import uz.wiut.eshop.service.CatalogService;

@Component
public class ProductFormValidator implements Validator {

    @Autowired
    protected MessageSource messageSource;
    @Autowired
    protected CatalogService catalogService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Product.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ProductForm product = (ProductForm) o;
        String sku = product.getSku();
        Product p = catalogService.getProductBySku(sku);
        if(p != null){
            errors.rejectValue("sku", "error.exists", new Object[]{sku}, "Product SKU "+sku+" already exists");
        }
    }
}
