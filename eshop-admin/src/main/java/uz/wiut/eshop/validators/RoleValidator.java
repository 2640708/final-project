package uz.wiut.eshop.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import uz.wiut.eshop.entity.Role;
import uz.wiut.eshop.security.SecurityService;

@Component
public class RoleValidator implements Validator {
    @Autowired
    protected MessageSource messageSource;
    @Autowired
    protected SecurityService securityService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Role.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        Role role = (Role) obj;
        String name = role.getName();
        Role roleByName = securityService.findRoleByName(name);
        if (roleByName != null) {
            errors.rejectValue("name", "error.exists",
                    new Object[]{name},
                    "Role " + name + " already exists");
        }
    }
}
