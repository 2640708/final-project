package uz.wiut.eshop.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import uz.wiut.eshop.entity.Role;
import uz.wiut.eshop.entity.User;
import uz.wiut.eshop.security.SecurityService;

@Component
public class UserValidator implements Validator {
    @Autowired
    protected MessageSource messageSource;
    @Autowired
    protected SecurityService securityService;


    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        User user = (User) obj;
        String email = user.getEmail();
        User userByEmail = securityService.findUserByEmail(email);
        if (userByEmail != null) {
            errors.rejectValue("email", "error.exists",
                    new Object[]{email},
                    "Email " + email + " already in use");
        }

    }
}
