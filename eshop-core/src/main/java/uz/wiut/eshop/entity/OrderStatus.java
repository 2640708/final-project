package uz.wiut.eshop.entity;

public enum OrderStatus
{
    NEW, IN_PROCESS, COMPLETED, FAILED
}
