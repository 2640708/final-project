package uz.wiut.eshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.wiut.eshop.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Category getByName(String name);
}
