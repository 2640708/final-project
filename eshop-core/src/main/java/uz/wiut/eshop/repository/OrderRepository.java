package uz.wiut.eshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.wiut.eshop.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Integer>
{
    Order findByOrderNumber(String orderNumber);
}
