package uz.wiut.eshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.wiut.eshop.entity.Permission;

public interface PermissionRepository extends JpaRepository<Permission, Integer> {
}
