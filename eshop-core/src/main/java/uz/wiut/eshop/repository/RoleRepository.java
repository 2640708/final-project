package uz.wiut.eshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.wiut.eshop.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}
