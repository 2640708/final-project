package uz.wiut.eshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.wiut.eshop.entity.User;

public interface UserRepository extends JpaRepository<User,Integer> {
    User findByEmail(String email);
}
