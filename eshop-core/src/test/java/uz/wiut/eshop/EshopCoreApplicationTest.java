package uz.wiut.eshop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uz.wiut.eshop.common.EmailService;

import static org.junit.Assert.*;
import javax.sql.DataSource;
import java.sql.SQLException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EshopCoreApplication.class)
public class EshopCoreApplicationTest {
    @Autowired
    DataSource dataSource;
    @Autowired
    EmailService emailService;

    @Test
    public void testDummy() throws SQLException
    {
        String schema = dataSource.getConnection().getCatalog();
        assertEquals("eshop", schema);
    }

    @Test
    public void testSendEmail()
    {
        emailService.sendEmail("00004492@bk.ru", "Eshop - Test Mail", "This is a test email from OCTOBE TRADE Eshop");
    }
}
