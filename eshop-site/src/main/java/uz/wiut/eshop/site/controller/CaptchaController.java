package uz.wiut.eshop.site.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.wiut.eshop.site.security.CaptchaService;


import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class CaptchaController {

    @Autowired
    private CaptchaService captchaService;

    @RequestMapping(value = "/captcha", method = RequestMethod.GET)
    public void getCaptcha(HttpSession session, HttpServletResponse response) {
        captchaService.getCaptcha(session, response);
    }
}
