package uz.wiut.eshop.site.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uz.wiut.eshop.site.model.Cart;
import uz.wiut.eshop.site.model.OrderDTO;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CheckoutController extends EshopSiteBaseController {
    @Override
    protected String getHeaderTitle() {
        return null;
    }
    @RequestMapping(value="/checkout", method=RequestMethod.GET)
    public String checkout(HttpServletRequest request, Model model)
    {
        OrderDTO order = new OrderDTO();
        model.addAttribute("order", order);
        Cart cart = getOrCreateCart(request);
        model.addAttribute("cart", cart);
        return "checkout";
    }
}
