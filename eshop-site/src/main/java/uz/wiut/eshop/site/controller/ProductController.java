package uz.wiut.eshop.site.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import uz.wiut.eshop.entity.Product;
import uz.wiut.eshop.service.CatalogService;
import uz.wiut.eshop.site.utils.WebUtils;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
public class ProductController extends EshopSiteBaseController {
    @Autowired
    protected CatalogService catalogService;

    @Override
    protected String getHeaderTitle() {
        return "Product";
    }

    @RequestMapping(value = "/products/images/{productId}", method = RequestMethod.GET)
    public void showProductImage(@PathVariable String productId, HttpServletRequest request, HttpServletResponse response) {
        try {
            //WebUtils.IMAGES_DIR = "D:/FINAL PROJECT/eshop/products/";
            FileSystemResource file = new FileSystemResource(WebUtils.IMAGES_DIR + productId + ".jpg");
            response.setContentType("image/jpg");
            org.apache.commons.io.IOUtils.copy(file.getInputStream(), response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/products/{sku}")
    public String product(@PathVariable String sku, Model model) {
        Product product = catalogService.getProductBySku(sku);
        model.addAttribute("product", product);
        return "product";
    }

    @RequestMapping("/products")
    public String searchProducts(@RequestParam(name = "q", defaultValue = "") String query, Model model) {
        List<Product> products = catalogService.searchProducts(query);
        model.addAttribute("products", products);
        return "products";
    }

    @RequestMapping("/404")
    public String notFound() {
        return "404";
    }

}
