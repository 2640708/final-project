package uz.wiut.eshop.site.security;

import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.OutputStream;
import java.util.Random;

@Service("captchaService")
public class CaptchaService {

    public void getCaptcha(HttpSession session, HttpServletResponse response) {
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(5);
        for (int i = 0; i < 5; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));

        int width = 120;
        int height = 40;

        String capstr = sb.toString();
        System.out.println(capstr);
        session.setAttribute("captcha", capstr);

        Color background = new Color(204, 204, 204);

        Color fbl = new Color(0, 100, 0);

        Font fnt = new Font("SansSerif", 1, 24);

        BufferedImage cpimg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        Graphics g = cpimg.createGraphics();

        g.setColor(background);

        g.fillRect(0, 0, width, height);

        g.setColor(fbl);

        g.setFont(fnt);


        g.drawString(capstr, 25, 30);

        g.setColor(background);

        g.drawLine(10, 27, 80, 7);

        g.drawLine(10, 22, 80, 28);

        try {



            response.setContentType("image/jpeg");

            OutputStream strm = response.getOutputStream();
            ImageIO.write(cpimg, "jpeg", strm);


            strm.close();




        } catch (Exception e) {
//        System.out.println("");
        }

    }
}
