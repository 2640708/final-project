package uz.wiut.eshop.site.validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import uz.wiut.eshop.entity.Customer;
import uz.wiut.eshop.service.CustomerService;

@Component
public class CustomerValidator implements Validator {

    @Autowired
    private CustomerService custmoerService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Customer.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Customer customer = (Customer) o;
        Customer customerByEmail = custmoerService.getCustomerByEmail(customer.getEmail());
        if (customerByEmail != null) {
            errors.rejectValue("email", "error.exists",
                    new Object[]{customer.getEmail()},
                    "Email " + customer.getEmail() + " already in use");
        }
    }
}
